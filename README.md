Este es el codigo base del proyecto de GOBDATA para le empresa TRACODA.

La construccion de este proyecto esta basado en el tutorial Tech With Tim - Django & React Tutorial en YouTube. En caso de alguna
pregunta se puede hacer referencia al contenido de ese tutorial.

Requisitos:
    - Python >= 3.8
    - Pip >= 20
    - Django >= 3.0
    - node >= 10.0

Para poder instalar los módulos base que se están usando, posicionarse dentro de la carpeta de la app de frontend y correr
el siguiente comandos

    npm i

Pasos a seguir para utilizar le codigo base:

1. Crear un nuevo entorno virtual con virtualenv 
    
    Ejemplo: python3 -m virtualenv <directorio>
    
    En caso de estar dentro de la carpeta principal "GOBDATASV", poner un punto "." sin comillas, en vez del directorio. Esto creara
    los archivos necesarios dentro de la carpeta actual.

    Una vez creado el entorno, inicializarlo con el comando:

    source bin/activate

2. Instalar los requerimientos en el nuevo virtualenv utilizando el archivo requirements.txt
    
    Ejemplo: pip install - r /ubicacion/del/archivo/requirements.txt

    Al ejecutar 'pip freeze' deberia poderse ver que ya estan instaladas las dependencias con las que se trabajaran en el proyecto.

3. Dentro de la carpeta donde esta ubicado el archivo manage.py ejecutar: makemigrations, migrate, createsuperuser.

    Ejemplo: python manage.py <comando>

4. Verificar que la version de node con la que se esta trabajando en la maquina corresponda con el archivo de node

    Dentro del archivo babel.config.json linea 17, modificar la version de node con la que se esta trabajando. El archivo esta
    dentro de la carpeta de la app frontend.

5. Para correr el servidor de node, posicionarse dentro de la carpeta de la app de frontend y correr en la terminal:

    npm run dev



Tener en cuenta que debido a que son dos "servidores" (backend y frontend) se deben correr en diferentes terminales cada servidor.

    Ejemplo:
        Terminal 1 ->  python manage.py runserver
        Terminal 2 ->  npm run dev