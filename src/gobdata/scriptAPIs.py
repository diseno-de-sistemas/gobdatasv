import requests
import json

#Estándares > Categorías
"""
url = 'https://www.transparencia.gob.sv/api/v1/standard_categories.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
   """

#Estándares > Marcos
"""
url = 'https://www.transparencia.gob.sv/api/v1/information_standard_frames.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Estándares de información
"""
url = 'https://www.transparencia.gob.sv/api/v1/information_standards.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Instituciones > Tipo
"""
url = 'https://www.transparencia.gob.sv/api/v1/institution_types.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Instituciones
"""
url = 'https://www.transparencia.gob.sv/api/v1/institutions.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Instituciones > Dependencias
"""
url = 'https://www.transparencia.gob.sv/api/v1/dependencies.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Asesores
"""
url = 'https://www.transparencia.gob.sv/api/v1/consultants.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Funcionarios
"""
url = 'https://www.transparencia.gob.sv/api/v1/officials.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Funcionarios > Comités, Juntas, Consejos y otros
"""
url = 'https://www.transparencia.gob.sv/api/v1/committees.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Inventarios > Articulos
"""
url = 'https://www.transparencia.gob.sv/api/v1/inventories.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Remuneraciones
"""
url = 'https://www.transparencia.gob.sv/api/v1/remunerations.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Remuneraciones > Unidades presupuestarias
"""
url = 'https://www.transparencia.gob.sv/api/v1/budgetary_units.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Notas aclaratorias
"""
url = 'https://www.transparencia.gob.sv/api/v1/disclaimers.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Viajes
"""
url = 'https://www.transparencia.gob.sv/api/v1/travels.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Obras en ejecución 
"""
url = 'https://www.transparencia.gob.sv/api/v1/executing_works.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Servicios > Categorías
"""
url = 'https://www.transparencia.gob.sv/api/v1/services_categories.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Servicios
"""
url = 'https://www.transparencia.gob.sv/api/v1/services.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Servicios > Pasos
"""
url = 'https://www.transparencia.gob.sv/api/v1/service_steps.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Mecanismos de participación    
"""
url = 'https://www.transparencia.gob.sv/api/v1/participation_mechanisms.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Documentos > Categorías
"""
url = 'https://www.transparencia.gob.sv/api/v1/document_categories.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

#Documentos
"""
url = 'https://www.transparencia.gob.sv/api/v1/documents.json'

response = requests.get(url)
if response.status_code == 200:
    #print(type(response.text))
    #print(response.text)
    print(type(response.json()))
    print(response.json())
    dict_data = json.loads(response.text)
    print(type(dict_data))
    """

