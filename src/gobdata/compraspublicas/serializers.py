from rest_framework import serializers
from .models import Institucion, UACI, Proceso, Empresa, Licitacion

class ProcesoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proceso
        fields = ('id', 'nombre', 'costo_base', 'estado', 'tipo_servicio', 'informacion_adicional', 'indicaciones_generales', 'objeto', 'id_institucion', 'id_uaci')

class InstitucionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institucion
        fields = ('id','nombre', 'direccion', 'telefono')

class UaciSerializer(serializers.ModelSerializer):
    class Meta:
        model = UACI
        fields = ('id','nombre', 'direccion', 'telefono', 'horario_atencion', 'nombre_contacto', 'telefono_contacto', 'fuente_financiamiento')

class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ('id', 'fecha_adquisicion', 'razon_social', 'telefono', 
        'telefono_fax', 'correo_electronico', 'nit', 'direccion', 'especializacion', 'informacion_adicional')

        
class LicitacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Licitacion
        fields = ('id', 'codigo', 'nombre',
        'fecha_publicacion', 'tipo', 'monto_otorgado', 'detalle_anios', 'duracion', 'numero_empleados', 'informacion_adicional', 'id_empresa')