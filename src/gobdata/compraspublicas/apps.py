from django.apps import AppConfig


class CompraspublicasConfig(AppConfig):
    name = 'compraspublicas'
    verbose_name = 'Compras y Adquisiciones Públicas'
