from django.urls import path, re_path
from .views import index

urlpatterns = [
    #re_path(r'^gobdata/', index),
    path('', index),
    path('controldetrabajolegislativo',index),
    path('controldeadquisicionespublicas',index),
    path('controldepersonasnaturalesjuridicas',index),
]