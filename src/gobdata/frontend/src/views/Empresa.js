import { func } from "prop-types";
import React, { useState, useEffect } from "react";
const axios = require("axios");

const postForm = (formData, setFormData, initialFormState, setEmpresas) => {
  axios
    .post("/api/personasnaturalesjuridicas/empresas/crear", formData, {
      headers: {
        "X-CSRFToken": csrftoken,
      },
    })
    .then((response) => {
      if (response.status === 201) {
        console.log("Elemento agregado.");
      }
      console.log(response);
    })
    .catch((error) => console.log(error))
    .then(() => {
      setFormData({ ...initialFormState });
      fetchEmpresas(setEmpresas);
    });
};

const fetchEmpresas = (setEmpresas) => {
  axios
    .get("/api/personasnaturalesjuridicas/empresas")
    .then((response) => setEmpresas(response.data))
    .catch((error) => console.log(error));
};

const Table = (props) => {
  const [idClicked, setIdClicked] = useState(-1);

  const handleOnClick = (id) => {
    setIdClicked(id);
  };

  const bodyTable = props.empresaList.map((empresa, index) => {
    return (
      <tr key={empresa.id}>
        <td>{empresa.fecha}</td>
        <td>{empresa.nombreRazon}</td>
        <td>{empresa.telefono}</td>
        <td>{empresa.correoElectronico}</td>
        <td>{empresa.nit}</td>
        <td>{empresa.direccion}</td>
        <td>{empresa.fax}</td>
        <td>{empresa.especializacion}</td>

        <td>
          <button
            type="button"
            className="btn btn-success btn-sm"
            onClick={(event) => handleOnClick(empresa.id)}
          >
            Editar
          </button>
          <button
            type="button"
            className="btn btn-danger btn-sm"
            onClick={(event) => handleOnClick(index)}
          >
            Eliminar
          </button>
        </td>
      </tr>
    );
  });
  return (
    <>
      <table className="table caption-top">
        <caption>Listado de Empresas</caption>
        <thead>
          <tr>
            <th scope="col">Fecha</th>
            <th scope="col">Nombre Razon</th>
            <th scope="col">Telefono</th>
            <th scope="col">Correo Electronico</th>
            <th scope="col">NIT</th>
            <th scope="col">Direccion</th>
            <th scope="col">Fax</th>
            <th scope="col">Especializacion</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>{bodyTable}</tbody>
      </table>
    </>
  );
};

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;

const Empresa = () => {
  const initialFormState = Object.freeze({
    fecha: new Date().toISOString().slice(0, 10),
    nombreRazon: "",
    telefono: "",
    fax: "",
    correoElectronico: "",
    nit: "",
    direccion: "",
    especializacion: "",
    informacionAdicional: "",
  });
  const [formData, setFormData] = useState({ ...initialFormState });
  const [empresa, setEmpresas] = useState([]);

  const handleOnChange = ({ target }) => {
    const { name, value } = target;
    setFormData({ ...formData, [name]: value });
  };

  useEffect(() => {
    fetchEmpresas(setEmpresas);
  }, []);

  const submitForm = () => {
    if (!formData.nombreRazon) {
      console.log("Debe completar el nombre");
    }
    if (!formData.telefono) {
      console.log("Debe completar el telefono");
    }
    if (!formData.correoElectronico) {
      console.log("Debe completar el correo electronico");
    }
    if (!formData.nit) {
      console.log("Debe completar el Nit");
    }
    if (!formData.direccion) {
      console.log("Debe completar la direccion");
    }
    if (!formData.especializacion) {
      console.log("Debe completar la especializacion");
    }
    postForm(formData, setFormData, initialFormState, setEmpresas);
    console.log(formData)
  };

  return (
    <>
      <h2>Mantenimiento Empresa</h2>
      <div className="row mt-3">
        <div className="col-md-6">
          <label htmlfor="nombreRazon" className="form-label">
            Nombre Razon
          </label>
          <input
            value={formData.nombreRazon}
            type="text"
            id="nombreRazon"
            name="nombreRazon"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
        <div className="col-md-6">
          <label htmlfor="telefono" className="form-label">
            Teléfono
          </label>
          <input
            value={formData.telefono}
            type="text"
            id="telefono"
            name="telefono"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
      </div>

      <div className="row mt-3">
        <div className="col-md-6">
          <label htmlfor="fax" className="form-label">
            Fax
          </label>
          <input
            value={formData.fax}
            type="text"
            id="fax"
            name="fax"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
        <div className="col-md-6">
          <label htmlfor="direccion" className="form-label">
            Direccion
          </label>
          <input
            value={formData.direccion}
            type="text"
            id="direccion"
            name="direccion"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
      </div>

      <div className="row mt-3">
        <div className="col-md-6">
          <label htmlfor="nit" className="form-label">
            NIT
          </label>
          <input
            value={formData.nit}
            type="text"
            id="nit"
            name="nit"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
        <div className="col-md-6">
          <label htmlfor="correoElectronico" className="form-label">
            Correo Electronico
          </label>
          <input
            value={formData.correoElectronico}
            type="text"
            id="correoElectronico"
            name="correoElectronico"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
      </div>

      <div className="row mt-3">
        <div className="col-md-6">
          <label htmlfor="especializacion" className="form-label">
            Especializacion
          </label>
          <input
            value={formData.especializacion}
            type="text"
            id="especializacion"
            name="especializacion"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
        <div className="col-md-6">
          <label htmlfor="informacionAdicional" className="form-label">
            Información Adicional
          </label>
          <input
            value={formData.informacionAdicional}
            type="text"
            id="informacionAdicional"
            name="informacionAdicional"
            className="form-control"
            onChange={(event) => handleOnChange(event)}
          />
        </div>
      </div>
      <button className="btn btn-primary" onClick={submitForm}>
        Agregar
      </button>

      <Table empresaList={empresa} />
    </>
  );
};

export default Empresa;
