import React from "react";
import { render } from "react-dom";
import NavBar from "./NavBar";
import Home from "../views/Home";
import Empresa from "../views/Empresa";

//import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


function CTL() {
  return <h2>En desarrollo...</h2>;
}

function CAP() {
  return <h2>En desarrollo...</h2>;
}

function NotFound() {
  return <h2>La pagina solicitada no existe</h2>;
}

const App = () => {
  return (
    <Router>
      <div>
        <NavBar />
        <div className="container" exact={true}>
          <Switch>
            <Route path="/" exact={true}>
              <Home />
            </Route>
            <Route path="/controldetrabajolegislativo" exact={true}>
              <CTL />
            </Route>
            <Route path="/controldeadquisicionespublicas" exact={true}>
              <CAP />
            </Route>
            <Route path="/controldepersonasnaturalesjuridicas" exact={true}>
              <Empresa />
            </Route>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
};

const appDiv = document.getElementById("app");
render(<App />, appDiv);
export default App;
