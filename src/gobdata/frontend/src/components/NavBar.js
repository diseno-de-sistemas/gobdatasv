import React from "react";

const NavBar = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            GOBDATA
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/"
                >
                  Inicio
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/controldetrabajolegislativo">
                  Control de Trabajo Legislativo
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/controldeadquisicionespublicas">
                  Control de Adquisiciones Públicas
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/controldepersonasnaturalesjuridicas">
                  Control de personas Naturales y Jurídicas
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default NavBar;
