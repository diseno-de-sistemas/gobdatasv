from django.db import models
from django.core.validators import RegexValidator

# Create your models here.

class PeriodoLegislatura(models.Model):
    #id_periodo = models.IntegerField()
    rango_fecha = models.CharField(max_length=11, validators=[RegexValidator(regex='^[0-9]\d{3}\s-\s[0-9]\d{3}$', message='Debe contener unicamente numeros separados por un guion y dos espacios.'),])

    def __str__(self):
        return self.rango_fecha
    class Meta:
        verbose_name_plural = 'Periodos de Legislaturas'

class TipoSesionPlenaria(models.Model):
    #id_tipo = models.IntegerField()
    texto_tipo = models.CharField(max_length=25, validators=[RegexValidator('^[a-zA-Z]\w+$', message='Debe contener solo letras y todo debe estar unido.')])

    def __str__(self):
        return self.texto_tipo
    class Meta:
        verbose_name_plural = 'Tipos de Sesiones Plenarias'

class SesionPlenaria(models.Model):
    numero = models.IntegerField() #este campo indica el numero de sesion plenaria dentro del periodo legislativo (el numero puede repetirse con otros registros ya que dependera del periodo legislativo)
    fecha = models.DateField() #este campo indica la fecha en que se dio dicha sesion plenaria
    tipo = models.ForeignKey(TipoSesionPlenaria, on_delete=models.CASCADE) #este campo indica el tipo de sesion plenaria
    periodo_legislatura = models.ForeignKey(PeriodoLegislatura, on_delete=models.CASCADE) #este campo indica el periodo de legislatura de cierta sesion plenaria en un periodo de tiempo

    def __str__(self):
        sesion = f"Sesion {self.numero} Legislatura {self.periodo_legislatura}"
        return sesion
    class Meta:
        verbose_name_plural = 'Sesiones Plenarias'

class TipoComision(models.Model):
    #id_tipo = models.IntegerField()
    texto_tipo = models.CharField(max_length=25, validators=[RegexValidator('^[a-zA-Z]\w+$', message='Debe contener solo letras y todo debe estar unido.')])

    def __str__(self):
        return self.texto_tipo
    class Meta:
        verbose_name_plural = 'Tipos de Comisiones'

class Comision(models.Model):
    #id_comision = models.CharField(max_length=36, primary_key=True, unique=True) #este campo indica el id unico de la comision el cual es sacado de la plataforma de la asamblea (se identifico que al recuperar los dictamenes de una comision dentro de un periodo legislativo en especifico, el servido devuelve un codigo unico para esa comision)
    nombre = models.CharField(max_length=100) #este campo indica el nombre oficial de la comision (https://www.asamblea.gob.sv/sites/default/files/documents/decretos/171117_072857304_archivo_documento_legislativo.pdf)
    tipo =models.ForeignKey(TipoComision, on_delete=models.CASCADE) #este campo indica el tipo de comision (Permanente, Transitoria, Ad-hoc, Especiales)

    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural = 'Comisiones'

class TipoDictamen (models.Model):
    #id_tipo = models.IntegerField()
    texto_tipo = models.CharField(max_length=150, validators=[RegexValidator('^[a-zA-Z]\w+$', message='Debe contener solo letras y todo debe estar unido.')])

    def __str__(self):
        return self.texto_tipo
    class Meta:
        verbose_name_plural = 'Tipos de Dictamenes'

class Dictamen(models.Model):
    #id_dictamen = models.CharField(max_length=36, primary_key=True, unique=True) #este campo indica el id unico en la BD de cada dictamen
    numero = models.IntegerField() #este campo indica el numero de dictamen dentro de cada comision (el numero puede repetirse con otros registros ya que dependera del periodo legislativo)
    tipo = models.ForeignKey(TipoDictamen, on_delete=models.CASCADE) #este campo indica el tipo de dictamen, el cual se sacara de la pagina de la asamblea
    extracto = models.TextField(max_length=15000) #este campo indica una descripcion del dictamen, el cual se sacara de la pagina de la asamblea
    id_comision = models.ForeignKey(Comision, default='Política', on_delete=models.CASCADE) #este campo es una llave foranea que hace referencia a la comision a la que pertenece el dictamen
    documento_link = models.CharField(max_length=150) #este campo indica el link para poder ver el documento en pdf del dictamen
    id_sesion_plenaria = models.ForeignKey(SesionPlenaria, on_delete=models.CASCADE) #este campo es una llave foranea que hace referencia a la sesion plenaria que pertenece el dictamen
    periodo_legislatura = models.ForeignKey(PeriodoLegislatura, on_delete=models.CASCADE) #este campo indica el periodo de legislatura de cierta asamblea legislativa en un periodo de tiempo

    class Meta:
        verbose_name_plural = 'Dictamenes'

class PartidoPolitico(models.Model):
    # id_partido = models.IntegerField()
    acronimo = models.CharField(max_length=10)
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.acronimo
    class Meta:
        verbose_name_plural = 'Partidos Politicos'

class Diputado(models.Model):
    nombre = models.CharField(max_length=150) #este campo almacenara el nombre completo del diputado
    id_comision = models.ForeignKey(Comision, default='Política', on_delete=models.CASCADE) #este campo es una llave foranea que hace referencia a la comision que pertenece el diputado
    periodo_legislatura = models.ForeignKey(PeriodoLegislatura, on_delete=models.CASCADE) #este campo indica el periodo de legislatura de cierta asamblea legislativa en un periodo de tiempo
    partido = models.ForeignKey(PartidoPolitico, on_delete=models.CASCADE) #este campo indica el partido politico al que el diputado pertence

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Diputados'

class RespuestaVotacion(models.Model):
    #id_respuesta = models.IntegerField()
    texto_respuesta = models.CharField(max_length=25, validators=[RegexValidator('^[a-zA-Z]\w+$', message='Debe contener solo letras y todo debe estar unido.')])

    def __str__(self):
        return self.texto_respuesta
    class Meta:
        verbose_name_plural = 'Respuestas a Votaciones '

class Votacion(models.Model):
    id_dictamen = models.ForeignKey(Dictamen, on_delete=models.CASCADE) #este campo es una llave foranea que hace referencia al dictamen de la votacion
    id_diputado = models.ForeignKey(Diputado, on_delete=models.CASCADE) #este campo es una llave foranea que hace referencia al diputado, ya que todos los diputados presentes deben votar
    respuesta = models.ForeignKey(RespuestaVotacion, on_delete=models.CASCADE) #este campo indica la respuesta que dio el diputado al momento de votar

    class Meta:
        verbose_name_plural = 'Votaciones'
