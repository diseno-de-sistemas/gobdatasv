from django.urls import path
from .views import (PeriodoLegislaturaView, TipoSesionPlenariaView, TipoComisionView, RespuestaVotacionView, TipoDictamenView, PartidoPoliticoView,
                    SesionPlenariaListarCrearView, ComisionListarCrearView, VotacionListarCrearView, DictamenListarCrearView, DiputadoListarCrearView,
                    SesionPlenariaActualizarEliminarView, ComisionActualizarEliminarView, VotacionActualizarEliminarView, DictamenActualizarEliminarView, DiputadoActualizarEliminarView)
# from .views import (SesionPlenariaCrearListarView, ComisionCrearListarView, VotacionCrearListarView, DictamenCrearListarView,
#                     DiputadoCrearListarView, SesionPlenariaCrearView, ComisionCrearView,

#                     VotacionCrearView, DictamenCrearView, DiputadoCrearView, 
#                     PeriodoLegislaturaView, TipoSesionPlenariaView, TipoComisionView,
#                     RespuestaVotacionView, TipoDictamenView, PartidoPoliticoView,
#                     SesionPlenariaActualizarView, ComisionActualizarView,
#                     VotacionActualizarView, DictamenActualizarView, DiputadoActualizarView,
#                     SesionPlenariaEliminarView, ComisionEliminarView,
#                     VotacionEliminarView, DictamenEliminarView, DiputadoEliminarView,)

urlpatterns = [
    #url para ver y crear registros    
    path('periodolegislatura', PeriodoLegislaturaView.as_view()),
    path('tiposesionplenaria', TipoSesionPlenariaView.as_view()),
    path('tipocomision', TipoComisionView.as_view()),
    path('respuestavotacion', RespuestaVotacionView.as_view()),
    path('tipodictamen', TipoDictamenView.as_view()),
    path('partidopolitico', PartidoPoliticoView.as_view()),
    

    path('sesionesplenarias', SesionPlenariaListarCrearView.as_view()),
    path('comisiones', ComisionListarCrearView.as_view()),
    path('votaciones', VotacionListarCrearView.as_view()),
    path('dictamenes', DictamenListarCrearView.as_view()),
    path('diputados', DiputadoListarCrearView.as_view()),   

    #urls para actualizar y eliminar registros
    path('sesionesplenarias/modificar/<int:id>', SesionPlenariaActualizarEliminarView.as_view()), 
    path('comisiones/modificar/<int:id>', ComisionActualizarEliminarView.as_view()), 
    path('votaciones/modificar/<int:id>', VotacionActualizarEliminarView.as_view()), 
    path('dictamenes/modificar/<int:id>', DictamenActualizarEliminarView.as_view()), 
    path('diputados/modificar/<int:id>', DiputadoActualizarEliminarView.as_view()), 
    
    #urls para actualizar y eliminar registros
    # path('sesionesplenarias/crear', SesionPlenariaCrearView.as_view()),
    # path('comisiones/crear', ComisionCrearView.as_view()),
    # path('votaciones/crear', VotacionCrearView.as_view()),
    # path('dictamenes/crear', DictamenCrearView.as_view()),
    # path('diputados/crear', DiputadoCrearView.as_view()),

    #urls para actualizar registros
    # path('sesionesplenarias/actualizar', SesionPlenariaActualizarView.as_view()),
    # path('comisiones/actualizar', ComisionActualizarView.as_view()),
    # path('votaciones/actualizar', VotacionActualizarView.as_view()),
    # path('dictamenes/actualizar', DictamenActualizarView.as_view()),
    # path('diputados/actualizar', DiputadoActualizarView.as_view()),

    #urls para eliminar registros
    # path('sesionesplenarias/eliminar', SesionPlenariaEliminarView.as_view()),
    # path('comisiones/eliminar', ComisionEliminarView.as_view()),
    # path('votaciones/eliminar', VotacionEliminarView.as_view()),
    # path('dictamenes/eliminar', DictamenEliminarView.as_view()),
    # path('diputados/eliminar', DiputadoEliminarView.as_view()),
]