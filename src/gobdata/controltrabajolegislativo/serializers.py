from rest_framework import serializers
from .models import (Comision, Diputado, Dictamen, SesionPlenaria, Votacion,
                    PeriodoLegislatura, TipoSesionPlenaria, TipoComision, 
                    TipoDictamen, PartidoPolitico, RespuestaVotacion)

class PeriodoLegislaturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeriodoLegislatura
        fields = ('id','rango_fecha')

class TipoSesionPlenariaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoSesionPlenaria
        fields = ('id', 'texto_tipo')

class SesionPlenariaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SesionPlenaria
        fields = ('id','numero','fecha','tipo','periodo_legislatura')
        

class TipoComisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoComision
        fields = ('id', 'texto_tipo')

class ComisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comision
        fields = ('id','nombre','tipo')
        
class TipoDictamenSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoDictamen
        fields = ('id','texto_tipo')

class DictamenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dictamen
        fields = ('id','numero','tipo','extracto','id_comision','documento_link','id_sesion_plenaria','periodo_legislatura')

class PartidoPoliticoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartidoPolitico
        fields = ('id', 'acronimo', 'nombre')

class DiputadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diputado
        fields = ('id','nombre','id_comision','partido','periodo_legislatura')

class RespuestaVotacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RespuestaVotacion
        fields = ('id', 'texto_respuesta')

class VotacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Votacion
        fields = ('id','id_dictamen','id_diputado','respuesta')