from django.apps import AppConfig


class ControltrabajolegislativoConfig(AppConfig):
    name = 'controltrabajolegislativo'
    verbose_name = 'Control de Trabajo Legislativo'
