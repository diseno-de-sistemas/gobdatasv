-- INSERT INTO "main"."controltrabajolegislativo_comision"
-- ("id_comision", "nombre", "tipo_id")
-- VALUES ('CAC2936E-DA2A-411C-9DD6-57FF658DCA2E', 'Politica', '1'),
--         ('1E9586AD-2858-42AC-887C-1CE4B036D2E5','Cultura y Educación','1'),
--         ('758C91BC-0BDE-4125-B331-1CAEF1DC3401','Salud','1'),
--         ('B665C0AF-9540-4562-954F-1FB341CF1BC2','Hacienda y Especial del Presupuesto','1'),
--         ('8A269EB1-9394-4E96-9FEF-EF955B5114A2','Economía','1'),
--         ('B683C15D-C036-4F4F-9AB1-3B550C21070B','Legislación y Puntos Constitucionales','1'),
--         ('318DF7AA-B1BD-4DED-B4D7-91D707C34D4E','Relaciones Exteriores, Integración Centroamericana y Salvadoreños en el Exterior','1'),
--         ('75291F8C-605A-4DA5-A505-D783C807CD83','Justicia y Derechos Humanos','1'),
--         ('8F8070EE-B2A8-4E3E-A096-85FB6596B517','Obras Públicas, Transporte y Vivienda','1'),
--         ('F3F98686-C1C4-4890-BC12-F881C6DBEFCB','Asuntos Municipales','1'),
--         ('749C8E3F-2A9D-47C9-93EB-DCFF486BED90','Defensa','1'),
--         ('FCF99852-95E8-43EA-9291-D6C4FC4C85D7','Seguridad Pública y Combate a la Narcoactividad','1'),
--         ('366DF694-912B-4335-B20E-251FCC33D32B','Medio Ambiente y Cambio Climático','1'),
--         ('791D2E34-38AF-407D-92C1-DB98CD963111','Trabajo y Previsión Social','1'),
--         ('7E7638B3-CD71-4631-933A-DFF38B4ACB6C','Juventud y Deporte','1'),
--         ('BA9969B2-7110-4507-A4BB-C1BB7519D215','Mujer y la Igualdad de Género','1'),
--         ('56967D47-E324-4876-AB60-C71BDA8F98CE','Comisión Agropecuaria','1'),
--         ('83980E07-2545-4924-B21E-1272E59194FC','Reformas Electorales y Constitucionales','1'),
--         ('8ABC7879-5C7B-4D4B-9A48-C0F36914B293','Financiera','1'),
--         ('22D5C20D-7196-46D6-AB56-014C95D13439','La Familia, Niñez, Adolescencia, Adulto Mayor y Personas con Discapacidad','1');

INSERT INTO "main"."controltrabajolegislativo_comision"
("id", "nombre", "tipo_id")
VALUES ('1', 'Politica', '1'),
        ('2','Cultura y Educación','1'),
        ('3','Salud','1'),
        ('4','Hacienda y Especial del Presupuesto','1'),
        ('5','Economía','1'),
        ('6','Legislación y Puntos Constitucionales','1'),
        ('7','Relaciones Exteriores, Integración Centroamericana y Salvadoreños en el Exterior','1'),
        ('8','Justicia y Derechos Humanos','1'),
        ('9','Obras Públicas, Transporte y Vivienda','1'),
        ('10','Asuntos Municipales','1'),
        ('11','Defensa','1'),
        ('12','Seguridad Pública y Combate a la Narcoactividad','1'),
        ('13','Medio Ambiente y Cambio Climático','1'),
        ('14','Trabajo y Previsión Social','1'),
        ('15','Juventud y Deporte','1'),
        ('16','Mujer y la Igualdad de Género','1'),
        ('17','Comisión Agropecuaria','1'),
        ('18','Reformas Electorales y Constitucionales','1'),
        ('19','Financiera','1'),
        ('20','La Familia, Niñez, Adolescencia, Adulto Mayor y Personas con Discapacidad','1');

INSERT INTO "main"."controltrabajolegislativo_sesionplenaria"
 ("id", "numero", "fecha","periodo_legislatura_id","tipo_id") 
 VALUES ('2','20','2021-09-14','1','1'),
        ('3','19','2021-09-07','1','1'),
        ('4','18','2021-08-31','1','1'),
        ('5','17','2021-08-24','1','1'),
        ('6','16','2021-08-17','1','1'),
        ('7','15','2021-08-10','1','1'),
        ('8','14','2021-07-27','1','1'),
        ('9','13','2021-07-20','1','1'),
        ('10','12','2021-07-13','1','1'),
        ('11','11','2021-07-06','1','1'),
        ('12','10','2021-06-30','1','1'),
        ('13','9','2021-06-22','1','1'),
        ('14','8','2021-06-15','1','1'),
        ('15','7','2021-06-08','1','1'),
        ('16','6','2021-06-04','1','1'),
        ('17','5','2021-05-25','1','1'),
        ('18','4','2021-05-18','1','1'),
        ('19','3','2021-05-11','1','1'),
        ('20','2','2021-05-05','1','1'),
        ('21','1','2021-08-06','1','2');

INSERT INTO "main"."controltrabajolegislativo_partidopolitico"
    ("id","acronimo","nombre")
    VALUES  ('1','FMLN','Frente Farabundo Marti para la Liberación Nacional'),
            ('2','ARENA','Alianza Republicana Nacionalista'),
            ('3','GANA','Gran Alianza por la Unidad Nacional'),
            ('4','PCN','Partido de Concertación Nacional'),
            ('5','CD','Cambio Democrático'),
            ('6','NT','Nuestro Tiempo'),
            ('7','VAMOS','Vamos'),
            ('8','PDC','Partido Social Demócrata Cristiano'),
            ('9','NI','Nuevas Ideas'),
            ('10','DS','Democracia Salvadoreña');

INSERT INTO "main"."controltrabajolegislativo_tiposesionplenaria"
    ("id","texto_tipo")
    VALUES  ('1','Ordinaria'),
            ('2','Extraordinaria');
            
INSERT INTO "main"."controltrabajolegislativo_periodolegislatura"
    ("id","rango_fecha")
    VALUES  ('1','2021-2024'),
            ('2','2018-2021'),
            ('3','2015-2018'),
            ('4','2012-2015');

INSERT INTO "main"."controltrabajolegislativo_respuestavotacion"
    ("id","texto_respuesta")
    VALUES  ('1','A favor'),
            ('2','En contra'),
            ('3','Abstinencia');

INSERT INTO "main"."controltrabajolegislativo_tipocomision"
    ("id","texto_tipo")
    VALUES  ('1','Permanente'),
            ('2','Transitoria'),
            ('3','Ad-hoc'),
            ('4','Especiales');

INSERT INTO "main"."controltrabajolegislativo_tipodictamen"
    ("id","texto_tipo")
    VALUES  ('1','Favorable'),
            ('2','Desfavorable'),
            ('3','Archivo'),
            ('4','Ratificando Proyecto de Decreto Legislativo'),
            ('5','Estableciendo fecha'),
            ('6','Recomendable');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('1', 'Ricardo Humberto Rivas Villanueva', '17', '9', '1'),
        ('2', 'José Serafín Orantes Rodriguez', '17', '4', '1'),
        ('3', 'Santos Adelmo Rivas Rivas', '17', '3', '1'),
        ('4', 'Norma Idalia Lobo Martel', '17', '9', '1'),
        ('5', 'Felipe Alfredo Martínez Interiano', '17', '9', '1'),
        ('6', 'Estuardo Ernesto Rodríguez Pérez', '17', '9', '1'),
        ('7', 'Saúl Enrique Mancia', '17', '9', '1'),
        ('8', 'Edgar Antonio Fuentes Guardado', '17', '9', '1'),
        ('9', 'Herbert Azael Rodas Díaz', '17', '9', '1'),
        ('10', 'Janneth Xiomara Molina', '17', '9', '1'),
        ('11', 'Reinaldo Alcides Carballo Carballo', '17', '8', '1'),
        ('12', 'Mauricio Roberto Linares Ramirez', '17', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('13', 'Elisa Marcela Rosales Ramírez', '10', '9', '1'),
        ('14', 'José Bladimir Barahona Hernández', '10', '9', '1'),
        ('15', 'Aronette Rebeca Mencia Díaz', '10', '9', '1'),
        ('16', 'Gerardo Balmore Aguilar Soriano', '10', '9', '1'),
        ('17', 'Jonathan Isaac Hernández Ramírez', '10', '9', '1'),
        ('18', 'Edgardo Antonio Meléndez Mulato', '10', '9', '1'),
        ('19', 'Numan Pompilio Salgado García', '10', '3', '1'),
        ('20', 'Rosa María Romero', '10', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('21', 'Katheryn Alexia Rivas González', '16', '9', '1'),
        ('22', 'Lorena Johanna Fuentes de Orantes', '16', '9', '1'),
        ('23', 'Norma Idalia Lobo Martel', '16', '9', '1'),
        ('24', 'Iris Ivonne Hernández González', '16', '9', '1'),
        ('25', 'Marcela Balbina Pineda Erazo', '16', '9', '1'),
        ('26', 'Ana Maricela Canales de Guardado', '16', '9', '1'),
        ('27', 'Marleni Esmeralda Funes Rivera', '16', '1', '1'),
        ('28', 'Marcela Guadalupe Villatoro Alvarado', '16', '2', '1'),
        ('29', 'Claudia Mercedes Ortiz Menjivar', '16', '7', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('30', 'Reinaldo Alcides Carballo Carballo', '2', '8', '1'),
        ('31', 'Rodrigo Javier Ayala Claros', '2', '9', '1'),
        ('32', 'Raúl Neftalí Castillo Rosales', '2', '9', '1'),
        ('33', 'Jonathan Isaac Hernández Ramírez', '2', '9', '1'),
        ('34', 'Elisa Marcela Rosales Ramírez', '2', '9', '1'),
        ('35', 'Ana Magdalena Figueroa Figueroa', '2', '9', '1'),
        ('36', 'Edwin Antonio Serpas Ibarra', '2', '9', '1'),
        ('37', 'Erick Alfredo García Salguero', '2', '9', '1'),
        ('38', 'Walter Amilcar Alemán Hernández', '2', '9', '1'),
        ('39', 'Santos Adelmo Rivas Rivas', '2', '3', '1'),
        ('40', 'Silvia Estela Ostorga de Escobar', '2', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('41', 'Juan Carlos Mendoza Portillo', '11', '3', '1'),
        ('42', 'Amilcar Giovanny Zaldaña Cáceres', '11', '9', '1'),
        ('43', 'Rubén Reynaldo Flores Escobar', '11', '9', '1'),
        ('44', 'Luis Armando Figueroa Rodríguez', '11', '9', '1'),
        ('45', 'José Ilofio García Torres', '11', '9', '1'),
        ('46', 'Francisco Alexander Guardado Deras', '11', '9', '1'),
        ('47', 'Jorge Alberto Castro Valle', '11', '9', '1'),
        ('48', 'Jaime Dagoberto Guevara Arguet11a', '11', '1', '1'),
        ('49', ' Rodrigo Ávila Avilés ', '11', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('50', 'Rodrigo Javier Ayala Claros', '5', '9', '1'),
        ('51', 'William Eulises Soriano Herrera', '5', '9', '1'),
        ('52', 'Estuardo Ernesto Rodríguez Pérez', '5', '9', '1'),
        ('53', 'Elisa Marcela Rosales Ramírez', '5', '9', '1'),
        ('54', 'Samuel Aníbal Martínez Rivas', '5', '9', '1'),
        ('55', 'Aronette Rebeca Mencia Díaz', '5', '9', '1'),
        ('56', 'Héctor Leonel Rodríguez Uceda', '5', '9', '1'),
        ('57', 'Juan Carlos Mendoza Portillo', '5', '3', '1'),
        ('58', 'Yolanda Anabel Belloso Salazar', '5', '1', '1'),
        ('59', 'Ana María Margarita Escobar López', '5', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('60', 'Dania Abigail González Rauda', '19', '9', '1'),
        ('61', 'Caleb Neftalí Navarro Rivera', '19', '9', '1'),
        ('62', 'William Eulises Soriano Herrera', '19', '9', '1'),
        ('63', 'Aronette Rebeca Mencia Díaz', '19', '9', '1'),
        ('64', 'Héctor Enrique Sales Salguero', '19', '9', '1'),
        ('65', 'José Bladimir Barahona Hernández', '19', '9', '1'),
        ('66', 'Romeo Alexander Auerbach Flores', '19', '3', '1'),
        ('67', 'Yolanda Anabel Belloso Salazar', '19', '1', '1'),
        ('68', 'Carlos Armando Reyes Ramos', '19', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('69', 'Suecy Beverley Callejas Estrada', '20', '9', '1'),
        ('70', 'Suni Saraí Cedillos de Interiano', '20', '9', '1'),
        ('71', 'Janneth Xiomara Molina', '20', '9', '1'),
        ('72', 'Gerardo Balmore Aguilar Soriano', '20', '9', '1'),
        ('73', 'Lorena Johanna Fuentes de Orantes', '20', '9', '1'),
        ('74', 'Rodil Amilcar Ayala Nerio', '20', '9', '1'),
        ('75', 'José Francisco Lira Alvarado', '20', '2', '1'),
        ('76', 'Claudia Mercedes Ortiz Menjivar', '20', '7', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('77', 'Christian Reynaldo Guevara Guadrón', '4', '9', '1'),
        ('78', 'William Eulises Soriano Herrera', '4', '9', '1'),
        ('79', 'Amilcar Giovanny Zaldaña Cáceres', '4', '9', '1'),
        ('80', 'Suecy Beverley Callejas Estrada', '4', '9', '1'),
        ('81', 'Marcela Balbina Pineda Erazo', '4', '9', '1'),
        ('82', 'Raúl Neftalí Castillo Rosales', '4', '9', '1'),
        ('83', 'Caleb Neftalí Navarro Rivera', '4', '9', '1'),
        ('84', 'Guillermo Antonio Gallegos Navarrete', '4', '3', '1'),
        ('85', 'José Serafín Orantes Rodriguez', '4', '9', '1'),
        ('86', 'Reinaldo Alcides Carballo Carballo', '4', '9', '1'),
        ('87', 'Donato Eugenio Vaquerano Rivas', '4', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('88', 'Reynaldo Antonio López Cardoza', '15', '4', '1'),
        ('89', 'Lorena Johanna Fuentes de Orantes', '15', '9', '1'),
        ('90', 'Héctor Leonel Rodríguez Uceda', '15', '9', '1'),
        ('91', 'Francisco Josué García Villatoro', '15', '9', '1'),
        ('92', 'Rubén Reynaldo Flores Escobar', '15', '9', '1'),
        ('93', 'Jonathan Isaac Hernández Ramírez', '15', '9', '1'),
        ('94', 'José Javier Palomo Nieto', '15', '2', '1'),
        ('95', 'Reinaldo Alcides Carballo Carballo', '15', '8', '1'),
        ('96', ' John Tennant Wright Sol ', '15', '6', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('97', 'Rebeca Aracely Santos de González', '8', '9', '1'),
        ('98', 'Samuel Aníbal Martínez Rivas', '8', '9', '1'),
        ('99', 'Jorge Alberto Castro Valle', '8', '9', '1'),
        ('100', 'Erick Alfredo García Salguero', '8', '9', '1'),
        ('101', 'Dennis Fernando Salinas Bermúdez', '8', '9', '1'),
        ('102', 'Walter David Coto Ayala', '8', '9', '1'),
        ('103', 'Suni Saraí Cedillos de Interiano', '8', '9', '1'),
        ('104', 'Salvador Alberto Chacón García', '8', '9', '1'),
        ('105', 'Romeo Alexander Auerbach Flores', '8', '3', '1'),
        ('106', 'Ricardo Ernesto Godoy Peñate', '8', '2', '1'),
        ('107', ' Claudia Mercedes Ortiz Menjivar ', '8', '7', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('108', 'Marcela Balbina Pineda Erazo', '6', '9', '1'),
        ('109', 'Rodrigo Javier Ayala Claros', '6', '9', '1'),
        ('110', 'Rebeca Aracely Santos de González', '6', '9', '1'),
        ('111', 'Cruz Evelyn Merlos Molina', '6', '9', '1'),
        ('112', 'Walter David Coto Ayala', '6', '9', '1'),
        ('113', 'Mauricio Edgardo Ortiz Cardona', '6', '9', '1'),
        ('114', 'Francisco Eduardo Amaya Benítez', '6', '9', '1'),
        ('115', 'Guillermo Antonio Gallegos Navarrete', '6', '3', '1'),
        ('116', 'Dina Yamileth Argueta Avelar', '6', '1', '1'),
        ('117', 'René Alfredo Portillo Cuadra', '6', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('118', 'Sandra Yanira Martínez Tobar', '13', '9', '1'),
        ('119', 'Santos Adelmo Rivas Rivas', '13', '3', '1'),
        ('120', 'Norma Idalia Lobo Martel', '13', '9', '1'),
        ('121', 'Carlos Hermann Bruch Cornejo', '13', '9', '1'),
        ('122', 'Herbert Azael Rodas Díaz', '13', '9', '1'),
        ('123', 'Ángel Josué Lobos Rodríguez', '13', '9', '1'),
        ('124', 'Raúl Neftalí Castillo Rosales', '13', '9', '1'),
        ('125', 'Iris Ivonne Hernández González', '13', '9', '1'),
        ('126', 'Dina Yamileth Argueta Avelar', '13', '1', '1'),
        ('127', 'Mauricio Roberto Linares Ramirez', '13', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('128', 'Salvador Alberto Chacón García', '9', '9', '1'),
        ('129', 'José Serafín Orantes Rodriguez', '9', '4', '1'),
        ('130', 'Saúl Enrique Mancia', '9', '9', '1'),
        ('131', 'Dania Abigail González Rauda', '9', '9', '1'),
        ('132', 'Ricardo Humberto Rivas Villanueva', '9', '9', '1'),
        ('133', 'Felipe Alfredo Martínez Interiano', '9', '9', '1'),
        ('134', 'José Asunción Urbina Alvarenga', '9', '9', '1'),
        ('135', 'Numan Pompilio Salgado García', '9', '3', '1'),
        ('136', 'Marleni Esmeralda Funes Rivera', '9', '1', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('137', 'Ernesto Alfredo Castro Aldana', '1', '9', '1'),
        ('138', 'Ana Magdalena Figueroa Figueroa', '1', '9', '1'),
        ('139', 'Jorge Alberto Castro Valle', '1', '9', '1'),
        ('140', 'Suecy Beverley Callejas Estrada', '1', '9', '1'),
        ('141', 'Caleb Neftalí Navarro Rivera', '1', '9', '1'),
        ('142', 'Katheryn Alexia Rivas González', '1', '9', '1'),
        ('143', 'Christian Reynaldo Guevara Guadrón', '1', '9', '1'),
        ('144', 'Guillermo Antonio Gallegos Navarrete', '3', '9', '1'),
        ('145', 'Juan Carlos Mendoza Portillo', '3', '9', '1'),
        ('146', 'Reynaldo Antonio López Cardoza', '1', '9', '1'),
        ('147', 'Reinaldo Alcides Carballo Carballo', '4', '9', '1'),
        ('148', 'Jaime Dagoberto Guevara Argueta', '8', '9', '1'),
        ('149', 'René Alfredo Portillo Cuadra', '2', '9', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('150', 'Ana Magdalena Figueroa Figueroa', '7', '9', '1'),
        ('151', 'Reynaldo Antonio López Cardoza', '7', '4', '1'),
        ('152', 'Walter Amilcar Alemán Hernández', '7', '9', '1'),
        ('153', 'Edgardo Antonio Meléndez Mulato', '7', '9', '1'),
        ('154', 'José Raúl Chamagua Noyola', '7', '9', '1'),
        ('155', 'Cruz Evelyn Merlos Molina', '7', '9', '1'),
        ('156', 'José Ilofio García Torres', '7', '9', '1'),
        ('157', 'Francisco Josué García Villatoro', '7', '9', '1'),
        ('158', 'Santos Adelmo Rivas Rivas', '7', '3', '1'),
        ('159', 'Marcela Guadalupe Villatoro Alvarado', '7', '1', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('160', 'Romeo Alexander Auerbach Flores', '18', '3', '1'),
        ('161', 'José Raúl Chamagua Noyola', '18', '9', '1'),
        ('162', 'Katheryn Alexia Rivas González', '18', '9', '1'),
        ('163', 'Rebeca Aracely Santos de González', '18', '9', '1'),
        ('164', 'Erick Alfredo García Salguero', '18', '9', '1'),
        ('165', 'Walter Amilcar Alemán Hernández', '18', '9', '1'),
        ('166', 'Edgar Antonio Fuentes Guardado', '18', '9', '1'),
        ('167', 'Estuardo Ernesto Rodríguez Pérez', '18', '9', '1'),
        ('168', 'Yolanda Anabel Belloso Salazar', '18', '1', '1'),
        ('169', ' Alberto Armando Romero Rodríguez ', '18', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('170', 'Rodil Amilcar Ayala Nerio', '3', '9', '1'),
        ('171', 'José Asunción Urbina Alvarenga', '3', '9', '1'),
        ('172', 'Edwin Antonio Serpas Ibarra', '3', '9', '1'),
        ('173', 'Juan Alberto Rodríguez Escobar', '3', '9', '1'),
        ('174', 'Jonathan Isaac Hernández Ramírez', '3', '9', '1'),
        ('175', 'Luis Armando Figueroa Rodríguez', '3', '9', '1'),
        ('176', 'Ana Maricela Canales de Guardado', '3', '9', '1'),
        ('177', 'Jorge Luis Rosales Ríos', '3', '2', '1'),
        ('178', 'John Tennant Wright Sol', '3', '5', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('179', 'Francisco Eduardo Amaya Benítez', '12', '9', '1'),
        ('180', 'Walter David Coto Ayala', '12', '9', '1'),
        ('181', 'Carlos Hermann Bruch Cornejo', '12', '9', '1'),
        ('182', 'Mauricio Edgardo Ortiz Cardona', '12', '9', '1'),
        ('183', 'Dennis Fernando Salinas Bermúdez', '12', '9', '1'),
        ('184', 'Francisco Josué García Villatoro', '12', '9', '1'),
        ('185', 'Amilcar Giovanny Zaldaña Cáceres', '12', '9', '1'),
        ('186', 'Guillermo Antonio Gallegos Navarrete', '12', '9', '1'),
        ('187', 'Rodrigo Ávila Avilés', '12', '2', '1'),
        ('188', 'John Tennant Wright Sol', '12', '6', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('189', 'Edgardo Antonio Meléndez Mulato', '14', '9', '1'),
        ('190', 'Cruz Evelyn Merlos Molina', '14', '9', '1'),
        ('191', 'Iris Ivonne Hernández González', '14', '9', '1'),
        ('192', 'Héctor Enrique Sales Salguero', '14', '9', '1'),
        ('193', 'Janneth Xiomara Molina', '14', '9', '1'),
        ('194', 'Ángel Josué Lobos Rodríguez', '14', '9', '1'),
        ('195', 'Rodil Amilcar Ayala Nerio', '14', '9', '1'),
        ('196', 'Reinaldo Alcides Carballo Carballo', '14', '8', '1'),
        ('197', 'Jaime Dagoberto Guevara Argueta', '14', '1', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('198', 'Sandra Yanira Martínez Tobar', '21', '9', '1'),
        ('199', 'Christian Reynaldo Guevara Guadrón', '21', '9', '1'),
        ('200', 'Mauricio Edgardo Ortiz Cardona', '21', '9', '1'),
        ('201', 'Norma Idalia Lobo Martel', '21', '9', '1'),
        ('202', 'Raúl Neftalí Castillo Rosales', '21', '9', '1'),
        ('203', 'Saúl Enrique Mancia', '21', '9', '1'),
        ('204', 'Walter David Coto Ayala', '21', '9', '1'),
        ('205', 'Reynaldo Antonio López Cardoza', '21', '4', '1'),
        ('206', 'Dina Yamileth Argueta Avelar', '21', '1', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('207', 'Katheryn Alexia Rivas González', '22', '9', '1'),
        ('208', 'Caleb Neftalí Navarro Rivera', '22', '9', '1'),
        ('209', 'Amilcar Giovanny Zaldaña Cáceres', '22', '9', '1'),
        ('210', 'Lorena Johanna Fuentes de Orantes', '22', '9', '1'),
        ('211', 'Carlos Hermann Bruch Cornejo', '22', '9', '1'),
        ('212', 'Reynaldo Antonio López Cardoza', '22', '4', '1'),
        ('213', 'Romeo Alexander Auerbach Flores', '22', '3', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('214', 'Jorge Alberto Castro Valle', '22', '9', '1'),
        ('215', 'Raúl Neftalí Castillo Rosales', '22', '9', '1'),
        ('216', 'Rebeca Aracely Santos de González', '22', '9', '1'),
        ('217', 'Walter David Coto Ayala', '22', '9', '1'),
        ('218', 'William Eulises Soriano Herrera', '22', '9', '1'),
        ('219', 'Marcela Balbina Pineda Erazo', '22', '9', '1'),
        ('220', 'Juan Carlos Mendoza Portillo', '22', '3', '1'),
        ('221', 'Rosa María Romero', '22', '2', '1');

INSERT INTO "main"."controltrabajolegislativo_diputado"
("id", "nombre", "id_comision_id", "partido_id", "periodo_legislatura_id")
VALUES  ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', ''),
        ('', '', '', '', '');

-- NO UTILIZAR ESTE, NO TIENE DATOS
INSERT INTO "main"."controltrabajolegislativo_dictamen"
 ("id","numero","extracto","documento_link","id_comision_id","id_sesion_plenaria_id","periodo_legislatura_id","tipo_id")
 VALUES ('1','','','','','','','',''),
        ('2','','','','','','','',''),
        ('3','','','','','','','',''),
        ('4','','','','','','','',''),
        ('5','','','','','','','',''),
        ('6','','','','','','','',''),
        ('7','','','','','','','',''),
        ('8','','','','','','','',''),
        ('9','','','','','','','',''),
        ('10','','','','','','','',''),
        ('11','','','','','','','',''),
        ('12','','','','','','','',''),
        ('13','','','','','','','',''),
        ('14','','','','','','','',''),
        ('15','','','','','','','',''),
        ('16','','','','','','','',''),
        ('17','','','','','','','',''),
        ('18','','','','','','','',''),
        ('19','','','','','','','',''),
        ('20','','','','','','','','');