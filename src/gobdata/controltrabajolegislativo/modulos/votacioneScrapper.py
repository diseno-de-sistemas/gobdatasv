#importacion de librerias que necesita el web scrapper
import requests
from bs4 import BeautifulSoup
import json

url = 'https://www.asamblea.gob.sv/index.php/agenda-legislativa/votaciones-ajax?start=0&length=4417' #url inicial, por el momento tiene 4417 registros en la plataforma
header={'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36'} #header necesario para que el sitioweb no detecte como robot el web scrapper

page=requests.get(url, headers = header) #se crea la peticion al sitio web
soup=BeautifulSoup(page.text, 'lxml') #parse del sitio web al beautifulsoup para extraer la info de la peticion


#Con un poco de inspección de la plataforma con las herramientas de desarrollador del navegador web, ser determinó que la petición para mostrar
#las votaciones, es una peticion ajax al servidor y devuelve un objeto json, el cual será utilzado para alimentar los datos de nuestra base de datos.

votaciones_json = json.loads(soup.text) #se guarda el texto en un objeto json

# with open('votaciones_json','w') as file:  #sentencia para guardar archivo json dentro de los modulos
#     json.dump(votaciones_json, file, indent=4)

json_keys=votaciones_json['data'][0].keys()
json_labels=[]
for key in json_keys:
    json_labels.append(key)
print(json_labels)