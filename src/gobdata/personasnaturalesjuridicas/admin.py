from django.contrib import admin
from personasnaturalesjuridicas.models import FuncionarioPublico, Empresa, Contrato

# Register your models here.

class FuncionarioPublicoAdmin(admin.ModelAdmin):
    list_display=('nombre', 'apellido')

class EmpresaAdmin(admin.ModelAdmin):
    list_display=('fecha','nombre_razon', 'telefono', 'fax', 'correo_electronico', 'nit', 'direccion', 'especializacion', 'informacion_adicional')

class ContratoAdmin(admin.ModelAdmin):
    list_display=('codigo', 'nombre','descripcion')

admin.site.register(FuncionarioPublico, FuncionarioPublicoAdmin)
admin.site.register(Empresa, EmpresaAdmin)
admin.site.register(Contrato, ContratoAdmin)