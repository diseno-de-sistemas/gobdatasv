from rest_framework import serializers
from .models import Empresa, Contrato, FuncionarioPublico

class FuncionarioPublicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FuncionarioPublico
        fields = ['id','nombre','apellido','id_empresa']

class EmpresaPNJSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ('id','fecha','nombre_razon', 'telefono', 'fax', 'correo_electronico', 'nit', 'direccion', 'especializacion', 'informacion_adicional')
        
class ContratoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contrato
        fields = ('id','codigo','nombre', 'id_empresa')