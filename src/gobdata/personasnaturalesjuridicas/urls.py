from django.urls import path
from .views import (FuncionarioPublicoListarCrearView, EmpresaListarCrearView, ContratoListarCrearView,
                    FuncionarioPublicoActualizarEliminarView, EmpresaActualizarEliminarView, ContratoActualizarEliminarView)

urlpatterns = [
    #url para ver registros
    path('funcionariospublicos', FuncionarioPublicoListarCrearView.as_view()),
    path('empresas', EmpresaListarCrearView.as_view()),
    path('contratos', ContratoListarCrearView.as_view()),
    
    
    #urls para actualizar y eliminar registros
    path('funcionariospublicos/modificar/<int:id>', FuncionarioPublicoActualizarEliminarView.as_view()),
    path('empresas/modificar/<int:id>', EmpresaActualizarEliminarView.as_view()),
    path('contratos/modificar/<int:id>', ContratoActualizarEliminarView.as_view()),

]