from django.shortcuts import render
from rest_framework import generics
from .models import Empresa, Contrato, FuncionarioPublico
from .serializers import EmpresaPNJSerializer, ContratoSerializer, FuncionarioPublicoSerializer
# Create your views here.

#Vistas para listar y crear los datos en la base de datos
class FuncionarioPublicoListarCrearView(generics.ListCreateAPIView):
    #queryset=FuncionarioPublico.objects.all()
    serializer_class = FuncionarioPublicoSerializer

    def get_queryset(self):
        return FuncionarioPublico.objects.all()

class EmpresaListarCrearView(generics.ListCreateAPIView):
    #queryset=Empresa.objects.all()
    serializer_class = EmpresaPNJSerializer

    def get_queryset(self):
        return Empresa.objects.all()

class ContratoListarCrearView(generics.ListCreateAPIView):
    #queryset=Contrato.objects.all()
    serializer_class = ContratoSerializer

    def get_queryset(self):
        return Contrato.objects.all()

#Vistas para actualizar y eliminar los datos en la base de datos
class FuncionarioPublicoActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset=FuncionarioPublico.objects.all()
    serializer_class = FuncionarioPublicoSerializer
    lookup_field = "id"

class EmpresaActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset=Empresa.objects.all()
    serializer_class = EmpresaPNJSerializer
    lookup_field = "id"

class ContratoActualizarEliminarView(generics.RetrieveUpdateDestroyAPIView):
    queryset=Contrato.objects.all()
    serializer_class = ContratoSerializer
    lookup_field = "id"