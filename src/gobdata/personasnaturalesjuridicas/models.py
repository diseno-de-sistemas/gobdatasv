from django.db import models

# Create your models here.

class Empresa(models.Model):
    fecha = models.DateField()
    nombre_razon = models.CharField(max_length=150)
    telefono = models.CharField(max_length=12)
    fax= models.CharField(max_length=25)
    correo_electronico = models.CharField(max_length=50)
    nit= models.CharField(max_length=30)
    direccion= models.CharField(max_length=100)
    especializacion = models.CharField(max_length=50)
    informacion_adicional =models.TextField(max_length=200)

class Contrato(models.Model):
    codigo = models.CharField(max_length=150)
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=360, null=True)
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)

class FuncionarioPublico(models.Model):
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    #Debe permitir valores nulos, investigar serializer FK
    id_empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
