from django.apps import AppConfig


class PersonasnaturalesjuridicasConfig(AppConfig):
    name = 'personasnaturalesjuridicas'
    verbose_name = 'Personas Naturales y Jurídicas'
